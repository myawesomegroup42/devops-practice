LOCAL_ENV := local
DEV_ENV := dev

DOCKER_COMPOSE = `which docker-compose`

BACKEND_CONTAINER ?= devops_backend
FRONTEND_CONTAINER ?= devops_frontend
DB_CONTAINER ?= devops_db

DEFAULT_SERVICES ?= frontend+backend+db

ALL_SERVICES_LIST := $(subst +, ,$(DEFAULT_SERVICES))
