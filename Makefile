include options.mk

########################################################################################
# Developer interface: Container management.
########################################################################################

dev.up: dev.up.$(DEFAULT_SERVICES)

dev.up.%: ## Bring up services and their dependencies.
	docker-compose up -d $$(echo $* | tr + " ")

dev.stop: ## Stop all running services.
	docker-compose stop

dev.stop.%: ## Stop specific services.
	docker-compose stop $$(echo $* | tr + " ")

dev.kill: ## Kill all running services.
	docker-compose stop

dev.kill.%: ## Kill specific services.
	docker-compose kill $$(echo $* | tr + " ")

dev.rm-stopped: ## Remove stopped containers. Does not affect running containers.
	docker-compose rm --force

deploy:
	docker rmi -f $(shell docker images -f 'dangling=true' -q) 2> /dev/null || true
	docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

dev.print-container.%: ## Get the ID of the running container for a given service.
	@echo $$(docker-compose ps -q $*)

########################################################################################
# Developer interface: App management.
########################################################################################

isort: ## run isort to sort imports in all Python files
	isort backend/

test:
	pytest -v

revision:
	docker-compose run $(BACKEND_CONTAINER) alembic revision --autogenerate -m "Added quiz table"

upgrade:
	docker-compose run $(BACKEND_CONTAINER) alembic upgrade head

stamp:
	docker-compose run $(BACKEND_CONTAINER) alembic stamp base


########################################################################################
# Developer interface: Runtime service management (logs, shells, ps, attach).
########################################################################################

dev.attach: _expects-service.dev.attach

dev.attach.%: ## Attach to the specified service container process for debugging & seeing logs.
	docker attach "$$(make --silent --no-print-directory dev.print-container.$*)"

dev.ps: ## View list of created services and their statuses.
	docker-compose ps

dev.logs: ## View logs from running containers.
	docker-compose logs -f

dev.logs.%: ## View the logs of the specified service container.
	docker-compose logs -f --tail=500 $*

dev.shell: _expects-service.dev.shell

dev.shell.frontend:
	docker exec -it $(FRONTEND_CONTAINER) /bin/sh

dev.shell.backend:
	docker exec -it $(BACKEND_CONTAINER) /bin/bash

dev.shell.db:
	docker exec -it $(DB_CONTAINER) psql -U dev dev

########################################################################################
# Support for "prefix-form" commands:
#     $service-$action instead of dev.$action.$services
# For example, the command:
#     make dev.attach.frontend
# can be expressed as:
#     make frontend-attach
# This form may be quicker to type and more amenable to tab-completion.
########################################################################################

$(addsuffix -up, $(ALL_SERVICES_LIST)): %-up: dev.up.%
$(addsuffix -attach, $(ALL_SERVICES_LIST)): %-attach: dev.attach.%
$(addsuffix -logs, $(ALL_SERVICES_LIST)): %-logs: dev.logs.%
$(addsuffix -stop, $(ALL_SERVICES_LIST)): %-stop: dev.stop.%
$(addsuffix -shell, $(ALL_SERVICES_LIST)): %-shell: dev.shell.%
$(addsuffix -print-container, $(ALL_SERVICES_LIST)): %-print-container: dev.print-container.%

########################################################################################
# Helper targets for other targets to use.
########################################################################################

_expects-service.%:
	@echo "'make $*' on its own has no effect."
	@echo "It expects a service as a suffix."
	@echo "For example:"
	@echo "    make $*.frontend"
